package net.jevring.l8smartlight;

/**
 * @author markus@jevring.net
 */
public enum NativeAppLightType {
	MULTI_COLOR(2),
	TROPICAL(2),
	GALAXY(3),
	AURORA(4);
	private final byte value;

	NativeAppLightType(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}
}
