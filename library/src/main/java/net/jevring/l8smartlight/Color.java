package net.jevring.l8smartlight;

/**
 * Represents a color. Inputs are ints to avoid having to cast whenever using values over 127.
 * Allows values between 0 and 255 inclusive.
 *
 * @author markus@jevring.net
 * @created 2022-01-12 20:32
 */
public record Color(int red, int green, int blue) {
	public Color {
		validate(red);
		validate(green);
		validate(blue);
	}

	private void validate(int component) {
		if (component < 0 || component > 255) {
			throw new IllegalArgumentException("Components must be between 0 and 255 inclusive. Was " + component);
		}
	}

	public int to4ByteARGB() {
		return to4ByteARGB(red, green, blue);
	}

	public static int to4ByteARGB(int red, int green, int blue) {
		// Technically alpha is always full, even if we don't have an alpha channel
		return 0xff000000 | ((red << 16) & 0x00ff0000) | ((green << 8) & 0x0000ff00) | (blue & 0x000000ff);
	}
}
