package net.jevring.l8smartlight;

/**
 * Notification events from the <a href="http://l8smartlight.com/dev/slcp/1.0/">SLCP 1.0</a>
 *
 * @author markus@jevring.net
 * @created 2022-01-15 15:56
 */
public enum NotificationEvent {
	ADDED(0),
	MODIFIED(1),
	REMOVED(2);

	private final byte value;

	NotificationEvent(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}
}
