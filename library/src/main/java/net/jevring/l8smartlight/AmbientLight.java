package net.jevring.l8smartlight;

/**
 * Ambient response.
 *
 * @author markus@jevring.net
 * @created 2022-01-29 14:04
 */
public record AmbientLight(short value, byte percentage, ResponseType type) {
}
