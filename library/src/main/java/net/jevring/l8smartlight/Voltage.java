package net.jevring.l8smartlight;

/**
 * Battery voltage (in millivolts) and battery level from 0 to 1.
 *
 * @author markus@jevring.net
 * @created 2022-01-12 22:06
 */
public record Voltage(int milliVolts, double batteryLevel) {
}
