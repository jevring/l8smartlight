package net.jevring.l8smartlight;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 * @created 2022-01-29 17:09
 */
public record SensorThresholds(short noiseMin, short noiseMax, short proximityMin, short proximityMax, short ambientMin, short ambientMax) {
}
