package net.jevring.l8smartlight;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 * @created 2022-01-29 14:12
 */
public record Versions(String firmwareVersion, String hardwareVersion, String bootloaderVersion, String userMemoryVersion) {
}
