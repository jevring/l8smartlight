package net.jevring.l8smartlight;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 * @created 2022-01-29 14:08
 */
public enum ResponseType {
	NOTIFICATION(1),
	RESPONSE(0);

	private final byte value;

	ResponseType(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}

	public static ResponseType fromValue(byte value) {
		for (ResponseType responseType : values()) {
			if (responseType.value == value) {
				return responseType;
			}
		}
		System.err.println("Unknown response type: " + value);
		return null;
	}
}
