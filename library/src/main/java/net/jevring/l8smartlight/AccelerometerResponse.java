package net.jevring.l8smartlight;

/**
 * Accelerometer responses 
 *
 * @author markus@jevring.net
 * @created 2022-01-29 13:53
 */
public record AccelerometerResponse(byte x, byte y, byte z, boolean upsideDown, Orientation orientation, boolean tap, boolean shake) {
}
