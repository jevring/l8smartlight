package net.jevring.l8smartlight;

/**
 * The charging status of the L8SmartLight.
 *
 * @author markus@jevring.net
 * @created 2022-01-29 13:55
 */
public enum ChargingStatus {
	NOT_CHARGING(0x06),
	CHARGING(0x0A),
	CHARGE_COMPLETE(0x0C),
	FAULT(0x0E);

	private final byte value;

	ChargingStatus(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}

	public static ChargingStatus fromValue(byte value) {
		for (ChargingStatus chargingStatus : values()) {
			if (chargingStatus.value == value) {
				return chargingStatus;
			}
		}
		System.err.println("Unknown charging status: " + value);
		return null;
	}
}
