package net.jevring.l8smartlight;

/**
 * The orientation of the L8SmartLight.
 *
 * @author markus@jevring.net
 * @created 2022-01-29 13:55
 */
public enum Orientation {
	UP(1),
	DOWN(2),
	LEFT(6),
	RIGHT(5);

	private final byte value;

	Orientation(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}

	public static Orientation fromValue(byte value) {
		for (Orientation orientation : values()) {
			if (orientation.value == value) {
				return orientation;
			}
		}
		System.err.println("Unknown orientation: " + value);
		return null;
	}
}
