package net.jevring.l8smartlight;

/**
 * Proximity sensor response.
 *
 * @author markus@jevring.net
 * @created 2022-01-29 14:04
 */
public record Proximity(short value, byte percentage, ResponseType type) {
}
