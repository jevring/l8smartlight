package net.jevring.l8smartlight;

import com.fazecast.jSerialComm.SerialPort;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HexFormat;

/**
 * A connection to the L8SmartLight via a serial port.
 *
 * @author markus@jevring.net
 * @created 2022-01-10 21:02
 */
public class L8Connection implements AutoCloseable {
	private static final boolean DEBUG = true;
	private static final byte HEADER_0 = (byte) 0xAA;
	private static final byte HEADER_1 = (byte) 0x55;
	private final SerialPort serialPort;
	private final OutputStream out;
	private final InputStream in;

	public L8Connection(String comPortName) throws IOException {
		this(SerialPort.getCommPort(comPortName));
	}

	public L8Connection(SerialPort serialPort) throws IOException {
		this.serialPort = serialPort;
		if (serialPort.openPort(1_000)) {
			serialPort.flushIOBuffers();
			serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 5_000, 0);
			out = serialPort.getOutputStream();
			in = serialPort.getInputStream();
			int available = in.available();
			if (available > 0) {
				System.out.println("Draining remaining " + available + " bytes from buffer");
				// drain it first
				readResponse();

				// The underlying library just has a thread that reads from the port and loads the data into a buffer.
				// That's how InputStream.available() is able to return != 0
			}
		} else {
			throw new IOException("Could not open port");
		}
	}

	public String getComPortName() {
		return serialPort.getSystemPortName();
	}

	/**
	 * Sends a command to the L8SmartLight. This is just the command and its parameters. This method takes care of headers and CRC.
	 *
	 * @param commandAndParameters the command and its parameters
	 * @throws IOException if the command can't be sent.
	 */
	void sendCommand(byte... commandAndParameters) throws IOException {
		sendCommand(commandAndParameters, 0, commandAndParameters.length);
	}

	private void sendCommand(byte[] input, int offset, int length) throws IOException {
		// header(2) + length(1) + payload(length) + checksum(1)
		int totalMessageLength = 2 + 1 + length + 1;
		byte[] message = new byte[totalMessageLength];
		message[0] = HEADER_0;
		message[1] = HEADER_1;
		message[2] = (byte) length;
		System.arraycopy(input, offset, message, 3, length);
		message[totalMessageLength - 1] = CRC8.ccitt(input, offset, length);
		out.write(message);
		out.flush();
		if (DEBUG) {
			System.out.printf("WRITE: [%s]%n", HexFormat.of().withPrefix("0x").withDelimiter(", ").withUpperCase().formatHex(message));
		}
	}

	/**
	 * Read a response from the l8smartlight, validate it, and strip headers and checksum and just return the payload.
	 * The payload will always be at least 1 byte long, if present. Not all commands have a response, but the ones that
	 * do will always have at least 1 byte of payload, which is the payload response type, for example {@link L8SmartLight#CMD_L8_NOTIFAPPS_NUM_RESPONSE}
	 * <p>
	 * todo: decide on and document the invalid crc behavior
	 *
	 * @return the payload byte array from the response. Always at least 1 byte long for commands with responses.
	 * @throws IOException if the response can't be read
	 */
	byte[] readResponse() throws IOException {
		// Max message length
		byte[] buffer = new byte[1 + 1 + 1 + 255 + 1];
		// readNBytes() will keep on reading in a loop until N bytes have been read, so we don't need to do that ourselves, when the port is being difficult

		in.readNBytes(buffer, 0, 3);
		if (buffer[0] != HEADER_0 || buffer[1] != HEADER_1) {
			throw new IOException("Invalid header. Got " + Arrays.toString(buffer));
		}
		// by now we will have read the whole header. Find out how much more to read.
		int payloadSize = Byte.toUnsignedInt(buffer[2]);
		int payloadSizePlusChecksum = payloadSize + 1;
		in.readNBytes(buffer, 3, payloadSizePlusChecksum);
		String crc = "";
		// The length here is the payload length, so we have to add 1 to get to the index just after it
		byte providedChecksum = buffer[(1 + 1 + 1  + payloadSize + 1) - 1];
		byte expectedChecksum = CRC8.ccitt(buffer, 3, payloadSize);
		if (expectedChecksum != providedChecksum) {
			// todo: if this ever happens, it's likely because our CRC algorithm is wrong.
			//System.err.println("Invalid CRC");
			crc = String.format("CRC BAD! Expected 0x%x but found 0x%x", expectedChecksum, providedChecksum);
		}
		if (DEBUG) {
			System.out.printf("READ:  [%s] %s%n",
			                  HexFormat.of().withPrefix("0x").withDelimiter(", ").withUpperCase().formatHex(buffer, 0, 2 + 1 + payloadSize + 1),
			                  crc);
		}
		byte[] response = new byte[payloadSize];
		System.arraycopy(buffer, 3, response, 0, payloadSize);
		return response;
	}

	@Override
	public void close() throws IOException {
		out.close();
		in.close();
		serialPort.closePort();
	}

}
