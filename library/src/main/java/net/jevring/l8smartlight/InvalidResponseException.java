package net.jevring.l8smartlight;

/**
 * The response did not have the format we expected.
 *
 * @author markus@jevring.net
 * @created 2022-01-12 22:20
 */
public class InvalidResponseException extends RuntimeException {
	public InvalidResponseException(String message) {
		super(message);
	}
}
