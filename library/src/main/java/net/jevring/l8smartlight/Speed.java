package net.jevring.l8smartlight;

/**
 * Speeds from the <a href="http://l8smartlight.com/dev/slcp/1.0/">SLCP 1.0</a>
 *
 * @author markus@jevring.net
 * @created 2022-01-12 20:35
 */
public enum Speed {
	FAST(0),
	MEDIUM(1),
	SLOW(2);
	private final byte value;

	Speed(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}
}
