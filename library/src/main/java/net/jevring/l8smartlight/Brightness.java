package net.jevring.l8smartlight;

/**
 * Brightness from the <a href="http://l8smartlight.com/dev/slcp/1.0/">SLCP 1.0</a>
 *
 * @author markus@jevring.net
 */
public enum Brightness {
	HIGH(0),
	MEDIUM(1),
	LOW(2);
	private final byte value;

	Brightness(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}
}
