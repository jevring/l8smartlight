package net.jevring.l8smartlight;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static net.jevring.l8smartlight.Utils.extractShort;

/**
 * API to send commands to an L8SmartLight.
 * <br>Implements <a href="http://l8smartlight.com/dev/slcp/1.0/">the SLCP 1.0 specification.</a>
 *
 * @author markus@jevring.net
 * @created 2022-01-12 20:24
 */
public class L8SmartLight {
	// Some API calls expect the color in RGB format, and others in RGB format, with seemingly no justification for what to use when.

	private static final byte CMD_OK = (byte) 0x00;
	// todo: make use of the 'command that failed' byte
	private static final byte CMD_ERR = (byte) 0xFF;
	private static final byte CMD_PING = (byte) 0x01;
	private static final byte CMD_PONG = (byte) 0x02;
	private static final byte CMD_L8_LED_SET = 0x43;
	private static final byte CMD_L8_MATRIX_SET = 0x44;
	private static final byte CMD_L8_MATRIX_OFF = (byte) 0x45;
	private static final byte CMD_L8_VOLTAGE_QUERY = (byte) 0x46;
	private static final byte CMD_L8_VOLTAGE_RESPONSE = (byte) 0x47;
	private static final byte CMD_L8_TEMP_QUERY = (byte) 0x48;
	private static final byte CMD_L8_TEMP_RESPONSE = (byte) 0x49;
	private static final byte CMD_L8_BOOTLOADER = (byte) 0x4A;
	private static final byte CMD_L8_SUPERLED_SET = (byte) 0x4B;
	private static final byte CMD_L8_ACC_QUERY = (byte) 0x4C;
	private static final byte CMD_L8_ACC_RESPONSE = (byte) 0x4D;
	private static final byte CMD_L8_UID_QUERY = (byte) 0x4E;
	private static final byte CMD_L8_UID_RESPONSE = (byte) 0x4F;
	private static final byte CMD_L8_AMBIENT_QUERY = (byte) 0x50;
	private static final byte CMD_L8_AMBIENT_RESPONSE = (byte) 0x51;
	private static final byte CMD_L8_PROX_QUERY = (byte) 0x52;
	private static final byte CMD_L8_PROX_RESPONSE = (byte) 0x53;
	private static final byte CMD_L8_VERSIONS_QUERY = (byte) 0x60;
	private static final byte CMD_L8_VERSIONS_RESPONSE = (byte) 0x61;
	private static final byte CMD_L8_BUTTON_QUERY = (byte) 0x62;
	private static final byte CMD_L8_BUTTON_RESPONSE = (byte) 0x63;
	private static final byte CMD_L8_MIC_QUERY = (byte) 0x64;
	private static final byte CMD_L8_MIC_RESPONSE = (byte) 0x65;
	private static final byte CMD_L8_VBUS_QUERY = (byte) 0x66;
	private static final byte CMD_L8_VBUS_RESPONSE = (byte) 0x67;
	private static final byte CMD_L8_MCUTEMP_QUERY = (byte) 0x68;
	private static final byte CMD_L8_MCUTEMP_RESPONSE = (byte) 0x69;
	private static final byte CMD_L8_STORE_L8Y = (byte) 0x6A;
	private static final byte CMD_L8_STORE_L8Y_RESPONSE = (byte) 0x6B;
	private static final byte CMD_L8_READ_L8Y = (byte) 0x6C;
	private static final byte CMD_L8_READ_L8Y_RESPONSE = (byte) 0x6D;
	private static final byte CMD_L8_SET_STORED_L8Y = (byte) 0x6E;
	private static final byte CMD_L8_DELETE_L8Y = (byte) 0x6F;
	private static final byte CMD_L8_STORE_FRAME = (byte) 0x70;
	private static final byte CMD_L8_STORE_FRAME_RESPONSE = (byte) 0x71;
	private static final byte CMD_L8_READ_FRAME = (byte) 0x72;
	private static final byte CMD_L8_READ_FRAME_RESPONSE = (byte) 0x73;
	private static final byte CMD_L8_DELETE_FRAME = (byte) 0x74;
	private static final byte CMD_L8_BATCHG_QUERY = (byte) 0x75;
	private static final byte CMD_L8_BATCHG_RESPONSE = (byte) 0x76;
	private static final byte CMD_L8_STORE_ANIM = (byte) 0x77;
	private static final byte CMD_L8_STORE_ANIM_RESPONSE = (byte) 0x78;
	private static final byte CMD_L8_READ_ANIM = (byte) 0x79;
	private static final byte CMD_L8_READ_ANIM_RESPONSE = (byte) 0x7A;
	private static final byte CMD_L8_DELETE_ANIM = (byte) 0x7B;
	private static final byte CMD_L8_PLAY_ANIM = (byte) 0x7C;
	private static final byte CMD_L8_STOP_ANIM = (byte) 0x7D;
	private static final byte CMD_L8_DELETE_USER_MEMORY = (byte) 0x7E;
	private static final byte CMD_L8_DISP_CHAR = (byte) 0x7F;
	private static final byte CMD_L8_SET_ORIENTATION = (byte) 0x80;
	private static final byte CMD_L8_APP_RUN = (byte) 0x81;
	private static final byte CMD_L8_APP_STOP = (byte) 0x82;
	private static final byte CMD_L8_SET_TEXT = (byte) 0x83;
	private static final byte CMD_L8_TRACE_MSG = (byte) 0x84;
	private static final byte CMD_L8_INIT_STATUS_QUERY = (byte) 0x85;
	private static final byte CMD_L8_SET_AUTOROTATE = (byte) 0x86;
	private static final byte CMD_L8_ORIENTATION_QUERY = (byte) 0x8A;
	private static final byte CMD_L8_ORIENTATION_RESPONSE = (byte) 0x8b;
	private static final byte CMD_L8_NUML8IES_QUERY = (byte) 0x8c;
	private static final byte CMD_L8_NUML8IES_RESPONSE = (byte) 0x8d;
	private static final byte CMD_L8_NUMANIMS_QUERY = (byte) 0x8E;
	private static final byte CMD_L8_NUMANIMS_RESPONSE = (byte) 0x8F;
	private static final byte CMD_L8_NUMFRAMES_QUERY = (byte) 0x90;
	private static final byte CMD_L8_NUMFRAMES_RESPONSE = (byte) 0x91;
	private static final byte CMD_L8_NOTIFAPP_STORE = (byte) 0x92;
	private static final byte CMD_L8_NOTIFAPP_QUERY = (byte) 0x93;
	private static final byte CMD_L8_NOTIFAPP_RESPONSE = (byte) 0x94;
	private static final byte CMD_L8_NOTIFAPPS_NUM_QUERY = (byte) 0x95;
	private static final byte CMD_L8_NOTIFAPPS_NUM_RESPONSE = (byte) 0x96;
	private static final byte CMD_L8_NOTIFAPP_ENABLE = (byte) 0x97;
	private static final byte CMD_L8_NOTIFAPP_DELETE = (byte) 0x98;
	private static final byte CMD_L8_SET_NOTIFICATION = (byte) 0x99;
	private static final byte CMD_L8_SET_LOW_BRIGHTNESS = (byte) 0x9A;
	private static final byte CMD_L8_FRAMEGRAB_QUERY = (byte) 0x9B;
	private static final byte CMD_L8_FRAMEGRAB_RESPONSE = (byte) 0x9C;
	private static final byte CMD_L8_POWEROFF = (byte) 0x9D;
	private static final byte CMD_L8_STATUSLEDS_ENABLE = (byte) 0x9E;
	private static final byte CMD_L8_NOISE_THRESHOLDS_SET = (byte) 0x9F;
	private static final byte CMD_L8_PROX_THRESHOLDS_SET = (byte) 0xA0;
	private static final byte CMD_L8_AMB_THRESHOLDS_SET = (byte) 0xA1;
	private static final byte CMD_L8_SENSORS_THRESHOLDS_QUERY = (byte) 0xA2;
	private static final byte CMD_L8_SENSORS_THRESHOLDS_RESPONSE = (byte) 0xA3;
	private static final byte CMD_L8_NOTIFAPPS_ENABLE_ALL = (byte) 0xA4;
	private static final byte CMD_L8_NOTIFAPPS_SILENCE = (byte) 0xA5;
	private static final byte CMD_L8_NOTIFAPPS_SILENCE_QUERY = (byte) 0xA6;
	private static final byte CMD_L8_NOTIFAPPS_SILENCE_RESPONSE = (byte) 0xA7;
	private static final byte NATIVE_APP_DICE = (byte) 0x00;
	private static final byte NATIVE_APP_PARTY = (byte) 0x01;
	private static final byte NATIVE_APP_LIGHT = (byte) 0x02;
	private static final byte NATIVE_APP_PROXIMITY_AND_AMBIENT_LIGHT = (byte) 0x03;

	private L8Connection connection;

	private L8SmartLight(L8Connection connection) {
		this.connection = connection;
	}

	/**
	 * Connects to an L8SmartLight on the specified COM port. This method will not return until connection was successful.
	 *
	 * @param serialPortName The name of the COM port. For example "COM4"
	 * @return a connected L8SmartLight.
	 * @see #attemptToReconnect()
	 */
	public static L8SmartLight connectToSerialPort(String serialPortName) {
		while (true) {
			try {
				return new L8SmartLight(new L8Connection(serialPortName));
			} catch (IOException e) {
				System.err.println("Could not connect to " + serialPortName);
			}
		}
	}

	/**
	 * Attempts to reconnect. Will first close the connection if possible.
	 * This method will not return until the connection was successful.
	 */
	public void attemptToReconnect() {
		try {
			connection.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (true) {
			try {
				connection = new L8Connection(connection.getComPortName());
				return;
			} catch (IOException e) {
				System.err.println("Could not reconnect to " + connection.getComPortName());
				e.printStackTrace();
			}
		}
	}

	// region DEVICE

	/**
	 * Sends a ping to the device.
	 *
	 * @return {@code true} if the device responded with pong. {@code false} if the device responded with anything else
	 * @throws IOException if communication with the device failed
	 */
	public boolean ping() throws IOException {
		connection.sendCommand(CMD_PING);
		byte[] response = readResponse();
		// this never actually returns PONG, it always just returns ERR on my device =(
		return response[0] == CMD_PONG;
	}

	public Voltage getBatteryInfo() throws IOException {
		byte[] response = exchange(CMD_L8_VOLTAGE_QUERY, CMD_L8_VOLTAGE_RESPONSE, 4);

		int batteryVoltageInMilliVolts = extractShort(response, 1);
		double batteryLevel = (double) response[3] / 0x64d;
		return new Voltage(batteryVoltageInMilliVolts, batteryLevel);
	}

	public ChargingStatus getChargingStatus() throws IOException {
		byte[] response = exchange(CMD_L8_BATCHG_QUERY, CMD_L8_BATCHG_RESPONSE, 2);
		return ChargingStatus.fromValue(response[1]);
	}

	/**
	 * Gets the 12-byte unique identifier.
	 */
	public byte[] getUid() throws IOException {
		connection.sendCommand(CMD_L8_UID_QUERY);
		return getResponseAndValidate(CMD_L8_UID_RESPONSE, 13);
	}

	public Versions getVersions() throws IOException {
		byte[] response = exchange(CMD_L8_VERSIONS_QUERY, CMD_L8_VERSIONS_RESPONSE, 10);
		return new Versions(String.format("%d.%02d.%02d", response[1], response[2], response[3]),
		                    String.format("%d.%02d", response[4], response[5]),
		                    String.format("%d.%02d", response[6], response[7]),
		                    String.format("%d.%02d", response[8], response[9]));
	}

	public double getVBusVoltage() throws IOException {
		byte[] response = exchange(CMD_L8_VBUS_QUERY, CMD_L8_VBUS_RESPONSE, 3);
		// Response is in millivolts
		return extractShort(response, 1) / 1000d;
	}

	public short getMcuTemperature() throws IOException {
		byte[] response = exchange(CMD_L8_MCUTEMP_QUERY, CMD_L8_MCUTEMP_RESPONSE, 3);
		return extractShort(response, 1);
	}

	public boolean isButtonPressed() throws IOException {
		byte[] response = exchange(CMD_L8_BUTTON_QUERY, CMD_L8_BUTTON_RESPONSE, 2);
		return response[1] == 1;
	}

	public Orientation getOrientation() throws IOException {
		connection.sendCommand(CMD_L8_ORIENTATION_QUERY);
		// This is the ONE TIME we don't want to skip the orientation response: when we're actually asking for it
		byte[] response = readResponse(false);
		if (response[0] != CMD_L8_ORIENTATION_RESPONSE && response.length != 2) {
			throw new InvalidResponseException(String.format("Expected response type %x but got %x, and length %d but got %d",
			                                                 CMD_L8_ORIENTATION_RESPONSE,
			                                                 response[0],
			                                                 2,
			                                                 response.length));
		}
		return Orientation.fromValue(response[1]);
	}

	public void enterBootLoader() throws IOException {
		connection.sendCommand(CMD_L8_BOOTLOADER);
	}

	public boolean clearScreen() throws IOException {
		connection.sendCommand(CMD_L8_MATRIX_OFF);
		return isOk(CMD_L8_MATRIX_OFF);
	}

	public void setBrightness(Brightness brightness) throws IOException {
		connection.sendCommand(CMD_L8_SET_LOW_BRIGHTNESS, brightness.getValue());
	}

	public void setOrientation(Orientation orientation) throws IOException {
		connection.sendCommand(CMD_L8_SET_ORIENTATION, orientation.getValue());
	}

	public void setAutoRotate(boolean enabled) throws IOException {
		connection.sendCommand(CMD_L8_SET_AUTOROTATE, (byte) (enabled ? 1 : 0));
	}

	public BufferedImage takeScreenshot() throws IOException {
		byte[] response = exchange(CMD_L8_FRAMEGRAB_QUERY, CMD_L8_FRAMEGRAB_RESPONSE, 129);
		return Utils.byteArray2ByteBGRToImage(response, 1);
	}

	public void setStatusLight(boolean enabled) throws IOException {
		connection.sendCommand(CMD_L8_STATUSLEDS_ENABLE, (byte) (enabled ? 1 : 0));
	}

	public void setNoiseThresholds(short min, short max) throws IOException {
		setThresholds(CMD_L8_NOISE_THRESHOLDS_SET, min, max);
	}

	public void setProximityThresholds(short min, short max) throws IOException {
		setThresholds(CMD_L8_PROX_THRESHOLDS_SET, min, max);
	}

	public void setAmbientLightThresholds(short min, short max) throws IOException {
		setThresholds(CMD_L8_AMB_THRESHOLDS_SET, min, max);
	}

	/**
	 * This will effectively factory reset the device.
	 */
	public boolean wipeUserMemory() throws IOException {
		connection.sendCommand(CMD_L8_DELETE_USER_MEMORY);
		return isOk(CMD_L8_DELETE_USER_MEMORY);
	}

	public void powerOff() throws IOException {
		connection.sendCommand(CMD_L8_POWEROFF);
	}


	// endregion

	// region SENSORS

	public double getTemperatureCelsius() throws IOException {
		byte[] response = exchange(CMD_L8_TEMP_QUERY, CMD_L8_TEMP_RESPONSE, 2);

		// index 0 is just the response code. 2 and 3 make up the actual value
		return (extractShort(response, 1)) / 10d;
	}

	public AccelerometerResponse getAccelerometerValues() throws IOException {
		byte[] response = exchange(CMD_L8_ACC_QUERY, CMD_L8_ACC_RESPONSE, 8);
		return new AccelerometerResponse(response[1],
		                                 response[2],
		                                 response[3],
		                                 response[4] == 1,
		                                 Orientation.fromValue(response[5]),
		                                 response[6] == 1,
		                                 response[7] == 1);
	}

	public short getNoiseLevel() throws IOException {
		byte[] response = exchange(CMD_L8_MIC_QUERY, CMD_L8_MIC_RESPONSE, 3);
		return extractShort(response, 1);
	}

	public AmbientLight getAmbientLight() throws IOException {
		byte[] response = exchange(CMD_L8_AMBIENT_QUERY, CMD_L8_AMBIENT_RESPONSE, 5);
		return new AmbientLight(extractShort(response, 2), response[3], ResponseType.fromValue(response[4]));
	}

	public Proximity getProximity() throws IOException {
		byte[] response = exchange(CMD_L8_PROX_QUERY, CMD_L8_PROX_RESPONSE, 5);
		return new Proximity(extractShort(response, 2), response[3], ResponseType.fromValue(response[4]));
	}

	public SensorThresholds getSensorThresholds() throws IOException {
		byte[] response = exchange(CMD_L8_SENSORS_THRESHOLDS_QUERY, CMD_L8_SENSORS_THRESHOLDS_RESPONSE, 13);
		return new SensorThresholds(extractShort(response, 1),
		                            extractShort(response, 3),
		                            extractShort(response, 5),
		                            extractShort(response, 7),
		                            extractShort(response, 9),
		                            extractShort(response, 11));

	}


	// endregion

	// region NOTIFICATIONS

	public int getNumberOfNotificationAppsStored() throws IOException {
		byte[] response = exchange(CMD_L8_NOTIFAPPS_NUM_QUERY, CMD_L8_NOTIFAPPS_NUM_RESPONSE, 2);
		return response[1];
	}

	public String getNotificationAppInformation(int notificationAppIndex) throws IOException {
		// todo: optional if it's extended or not
		connection.sendCommand(CMD_L8_NOTIFAPP_QUERY, (byte) notificationAppIndex, (byte) 0);
		byte[] response = getResponseAndValidate(CMD_L8_NOTIFAPP_RESPONSE, 3);
		int appStringLength = response[1];
		String appBundleName = new String(response, 2, appStringLength);
		boolean enabled = response[2 + appStringLength] == 1;

		// todo: better api
		return appBundleName;
	}

	public boolean storeNotification(String name, Color superLedColor, boolean enabled, BufferedImage image) throws IOException {
		byte[] command = new byte[1 + 1 + name.length() + 128 + 3 + 1];
		command[0] = CMD_L8_NOTIFAPP_STORE;
		command[1] = (byte) name.length();
		System.arraycopy(name.getBytes(StandardCharsets.UTF_8), 0, command, 2, name.length());

		// BGR colors for the image
		int c = 2 + name.length();
		c = Utils.imageTo2ByteBGRByteArray(image, command, c);
		// super led color
		command[c++] = (byte) superLedColor.red();
		command[c++] = (byte) superLedColor.green();
		command[c++] = (byte) superLedColor.blue();
		// enabled
		command[c++] = (byte) (enabled ? 0x01 : 0x00);
		connection.sendCommand(command);
		return isOk(CMD_L8_NOTIFAPP_STORE);
	}

	public void switchNotificationEnabledState(int index) throws IOException {
		// todo: this is a shitty api. It should just accept a byte for enabled/disabled, like the others, but it just flips a bit =(
		//  Should we make our function take a boolean, and just check first?
		connection.sendCommand(CMD_L8_NOTIFAPP_ENABLE, (byte) index);
	}

	public boolean showStoredNotification(String name, NotificationEvent notificationEvent, NotificationCategory notificationCategory) throws IOException {
		// todo: document that it doesn't seem to matter what event and category we send for custom ones
		byte[] command = new byte[1 + 1 + name.length() + 1 + 1];
		command[0] = CMD_L8_SET_NOTIFICATION;
		command[1] = (byte) name.length();
		System.arraycopy(name.getBytes(StandardCharsets.UTF_8), 0, command, 2, name.length());
		command[2 + name.length()] = notificationEvent.getValue();
		command[2 + name.length() + 1] = notificationCategory.getValue();

		connection.sendCommand(command);
		return isOk(CMD_L8_SET_NOTIFICATION);
	}

	public boolean deleteStoredNotification(byte notificationId) throws IOException {
		connection.sendCommand(CMD_L8_NOTIFAPP_DELETE, notificationId);
		return isOk(CMD_L8_NOTIFAPP_DELETE);
	}

	public void setAllNotifications(boolean enabled) throws IOException {
		connection.sendCommand(CMD_L8_NOTIFAPPS_ENABLE_ALL, (byte) (enabled ? 1 : 0));
	}

	public void setAllNotificationsSilenced(boolean silenced) throws IOException {
		connection.sendCommand(CMD_L8_NOTIFAPPS_SILENCE, (byte) (silenced ? 1 : 0));
	}

	public boolean isNotificationSilenced(int index) throws IOException {
		connection.sendCommand(CMD_L8_NOTIFAPPS_SILENCE_QUERY, (byte) index);
		byte[] response = getResponseAndValidate(CMD_L8_NOTIFAPPS_SILENCE_RESPONSE, 2);
		return response[1] == 1;
	}

	//endregion

	// region IMAGES

	public int storeImage(BufferedImage image) throws IOException {
		if (image.getWidth() > 8 || image.getHeight() > 8) {
			throw new IllegalArgumentException("Image cannot be larger than 8x8");
		}
		byte[] command = new byte[1 + 128];
		command[0] = CMD_L8_STORE_L8Y;
		Utils.imageTo2ByteBGRByteArray(image, command, 1);
		connection.sendCommand(command);
		byte[] response = getResponseAndValidate(CMD_L8_STORE_L8Y_RESPONSE, 2);
		return response[1];
	}

	public BufferedImage getStoredImage(int index) throws IOException {
		connection.sendCommand(CMD_L8_READ_L8Y, (byte) index);
		byte[] response = getResponseAndValidate(CMD_L8_READ_L8Y_RESPONSE, 129);
		return Utils.byteArray2ByteBGRToImage(response, 1);
	}

	public boolean showStoredImage(int index) throws IOException {
		connection.sendCommand(CMD_L8_SET_STORED_L8Y, (byte) index);
		return isOk(CMD_L8_SET_STORED_L8Y);
	}

	public boolean deleteStoredImage(int index) throws IOException {
		connection.sendCommand(CMD_L8_DELETE_L8Y, (byte) index);
		return isOk(CMD_L8_DELETE_L8Y);
	}

	public int getNumberOfStoredImages() throws IOException {
		byte[] response = exchange(CMD_L8_NUML8IES_QUERY, CMD_L8_NUML8IES_RESPONSE, 2);
		return Byte.toUnsignedInt(response[1]);
	}

	// endregion

	// region ANIMATIONS

	public int storeAnimationFrame(BufferedImage image) throws IOException {
		if (image.getWidth() > 8 || image.getHeight() > 8) {
			throw new IllegalArgumentException("Image cannot be larger than 8x8");
		}
		byte[] command = new byte[1 + 128];
		command[0] = CMD_L8_STORE_FRAME;
		Utils.imageTo2ByteBGRByteArray(image, command, 1);
		connection.sendCommand(command);
		byte[] response = getResponseAndValidate(CMD_L8_STORE_FRAME_RESPONSE, 2);
		return Byte.toUnsignedInt(response[1]);
	}

	public BufferedImage readAnimationFrame(int index) throws IOException {
		connection.sendCommand(CMD_L8_READ_FRAME, (byte) index);
		byte[] response = getResponseAndValidate(CMD_L8_READ_FRAME_RESPONSE, 129);
		return Utils.byteArray2ByteBGRToImage(response, 1);
	}

	public boolean deleteAnimationFrame(int index) throws IOException {
		connection.sendCommand(CMD_L8_DELETE_FRAME, (byte) index);
		return isOk(CMD_L8_DELETE_FRAME);
	}

	/**
	 * Stores an animation referencing animation frames and the duration of each frame, given in tens of seconds, as [frame, duration] pairs.
	 *
	 * @param frameAndTimingPairs frame and duration pairs. Must be even
	 * @return the animation index
	 */
	public int storeAnimation(int... frameAndTimingPairs) throws IOException {
		byte[] command = new byte[1 + 1 + frameAndTimingPairs.length];
		command[0] = CMD_L8_STORE_ANIM;
		command[1] = (byte) (frameAndTimingPairs.length / 2);

		// We have to use this loop because System.arraycopy() can't automatically cast from int to byte
		for (int i = 0; i < frameAndTimingPairs.length; i++) {
			command[2 + i] = (byte) frameAndTimingPairs[i];
		}
		connection.sendCommand(command);
		byte[] response = getResponseAndValidate(CMD_L8_STORE_ANIM_RESPONSE, 2);
		return Byte.toUnsignedInt(response[1]);
	}

	public int[] readAnimation(int index) throws IOException {
		connection.sendCommand(CMD_L8_READ_ANIM, (byte) index);
		byte[] response = readResponse();
		if (response[0] != CMD_L8_READ_ANIM_RESPONSE) {
			throw new InvalidResponseException(String.format("Expected response type %x but got %x", CMD_L8_READ_ANIM_RESPONSE, response[0]));
		}
		int numberOfFrameAndTimingPairs = response[1];
		int[] frameAndTimingPairs = new int[numberOfFrameAndTimingPairs * 2];
		for (int i = 0; i < numberOfFrameAndTimingPairs * 2; i += 2) {
			frameAndTimingPairs[i] = response[2 + i];
			frameAndTimingPairs[i + 1] = response[2 + i + 1];
		}
		return frameAndTimingPairs;
	}

	public void playAnimation(int index, boolean loop) throws IOException {
		connection.sendCommand(CMD_L8_PLAY_ANIM, (byte) index, (byte) (loop ? 1 : 0));
	}

	public void stopAnimation() throws IOException {
		connection.sendCommand(CMD_L8_STOP_ANIM);
		// todo: read response?
	}

	public boolean deleteAnimation(int index) throws IOException {
		connection.sendCommand(CMD_L8_DELETE_ANIM, (byte) index);
		return isOk(CMD_L8_DELETE_ANIM);
	}

	public int getNumberOfAnimationsStored() throws IOException {
		byte[] response = exchange(CMD_L8_NUMANIMS_QUERY, CMD_L8_NUMANIMS_RESPONSE, 2);
		return Byte.toUnsignedInt(response[1]);
	}

	public int getNumberOfStoredAnimationFrames() throws IOException {
		byte[] response = exchange(CMD_L8_NUMFRAMES_QUERY, CMD_L8_NUMFRAMES_RESPONSE, 2);
		return Byte.toUnsignedInt(response[1]);
	}

	// endregion

	// region DIRECT

	/**
	 * Displays the provided image.
	 *
	 * @param image the image to display. Should be max 8x8
	 * @throws IllegalArgumentException if the image is larger than 8x8
	 */
	public boolean showImage(BufferedImage image) throws IllegalArgumentException, IOException {
		if (image.getWidth() > 8 || image.getHeight() > 8) {
			throw new IllegalArgumentException("Image cannot be larger than 8x8");
		}
		byte[] command = new byte[1 + 128];
		command[0] = CMD_L8_MATRIX_SET;
		Utils.imageTo2ByteBGRByteArray(image, command, 1);
		connection.sendCommand(command);
		return isOk(CMD_L8_MATRIX_SET);
	}


	/**
	 * Displays a scrolling text. If {@code loop} is {@code true}, then {@link #stopApp()} can be called to stop the scrolling text.
	 *
	 * @param text  The text to display
	 * @param color The color of the text
	 * @param speed The speed of the scrolling
	 * @param loop  Whether the text should loop after finishing
	 */
	public void setText(String text, Color color, Speed speed, boolean loop) throws IOException {
		byte[] commandAndParameters = new byte[1 + 1 + 1 + 3 + text.length()];
		commandAndParameters[0] = CMD_L8_SET_TEXT;
		commandAndParameters[1] = (byte) (loop ? 1 : 0);
		commandAndParameters[2] = speed.getValue();
		commandAndParameters[3] = (byte) color.red();
		commandAndParameters[4] = (byte) color.green();
		commandAndParameters[5] = (byte) color.blue();
		for (int i = 0; i < text.length(); i++) {
			commandAndParameters[6 + i] = (byte) text.charAt(i);
		}
		connection.sendCommand(commandAndParameters);
	}

	public void setLed(int x, int y, Color color) throws IOException {
		// For some reason this just allows 16 levels of color for each component, so it has to be scaled down
		connection.sendCommand(CMD_L8_LED_SET, (byte) x, (byte) y, (byte) (color.blue() / 16), (byte) (color.green() / 16), (byte) (color.red() / 16));
	}

	public void setSuperLed(Color color) throws IOException {
		byte[] commandAndParameters = new byte[1 + 3];
		commandAndParameters[0] = CMD_L8_SUPERLED_SET;
		commandAndParameters[1] = (byte) color.blue();
		commandAndParameters[2] = (byte) color.green();
		commandAndParameters[3] = (byte) color.red();
		connection.sendCommand(commandAndParameters);
		// The docs don't specify a response, but apparently one is provided.
		// It looks like it's an OK, but with an extra byte.
		// I expected this to return no result, like setText(), but apparently there is a result here that we have to consume
		readResponse();
	}

	public void setCharacter(char character) throws IOException {
		// todo: this allows us to shift the location of this character, but the API for doing so seems super weird, so we might just support displaying the character itself.
		connection.sendCommand(CMD_L8_DISP_CHAR, (byte) character, (byte) 0);
	}

	// endregion

	// region NATIVE
	public void startNativeAppDice(Color color) throws IOException {
		connection.sendCommand(CMD_L8_APP_RUN, NATIVE_APP_DICE, (byte) color.blue(), (byte) color.green(), (byte) color.red());
	}

	public void startNativeAppParty() throws IOException {
		connection.sendCommand(CMD_L8_APP_RUN, NATIVE_APP_PARTY);
	}

	public void startNativeAppLight(NativeAppLightType lightType, int speedInTensOfMilliseconds, boolean backLedInverted) throws IOException {
		connection.sendCommand(CMD_L8_APP_RUN, NATIVE_APP_LIGHT, lightType.getValue(), (byte) speedInTensOfMilliseconds, (byte) (backLedInverted ? 1 : 0));
	}

	public void startNativeAppProximityAndAmbientLight(Color matrixColor,
	                                                   Color superLedColor,
	                                                   int threshold,
	                                                   ProximityAndAmbientLightSensorType sensorType) throws IOException {
		connection.sendCommand(CMD_L8_APP_RUN,
		                       NATIVE_APP_PROXIMITY_AND_AMBIENT_LIGHT,
		                       (byte) matrixColor.blue(),
		                       (byte) matrixColor.green(),
		                       (byte) matrixColor.red(),
		                       (byte) superLedColor.blue(),
		                       (byte) superLedColor.green(),
		                       (byte) superLedColor.red(),
		                       (byte) threshold,
		                       sensorType.getValue());
	}

	public void stopApp() throws IOException {
		connection.sendCommand(CMD_L8_APP_STOP);
		readResponse();
	}

	// endregion

	// todo: handle the TRACE messages that can come from the l8, presumably at any time

	//  This is also related to  CMD_L8_INIT_STATUS_QUERY which isn't entirely clear.


	private void setThresholds(byte thresholdCommand, short min, short max) throws IOException {
		byte[] command = new byte[5];
		command[0] = thresholdCommand;

		// todo: verify that this is the right order
		command[1] = (byte) ((min & 0xff00) >> 8);
		command[2] = (byte) (min & 0xff);
		command[3] = (byte) ((max & 0xff00) >> 8);
		command[4] = (byte) (max & 0xff);

		connection.sendCommand(command);
	}

	private byte[] readResponse() throws IOException {
		return readResponse(true);
	}

	/**
	 * @param skipOrientationResponse Sometimes you will get arbitrary orientation responses. Not as a response to any query, but more like notifications.
	 *                                They show up when you least expect it, so it's handy to have a response reader where you can strip these out, as they
	 *                                are almost always unwanted, except when you're actually asking for the orientation
	 */
	private byte[] readResponse(boolean skipOrientationResponse) throws IOException {
		byte[] response = connection.readResponse();
		if (response[0] == CMD_L8_ORIENTATION_RESPONSE && skipOrientationResponse) {
			// todo: we might want to skip any notifications here, actually. Not just the orientation response
			System.err.println("Found and skipped orientation response");
			return readResponse(true);
		}
		return response;
	}

	private byte[] getResponseAndValidate(byte expectedResponseType, int expectedResponseLength) throws IOException {
		byte[] response = readResponse();
		if (response[0] != expectedResponseType && response.length != expectedResponseLength) {
			throw new InvalidResponseException(String.format("Expected response type %x but got %x, and length %d but got %d",
			                                                 expectedResponseType,
			                                                 response[0],
			                                                 expectedResponseLength,
			                                                 response.length));
		}
		return response;
	}

	private boolean isOk(byte executedCommand) throws IOException {
		byte[] response = readResponse();
		return response[0] == CMD_OK && response[1] == executedCommand;
	}

	private byte[] exchange(byte command, byte expectedResponseType, int expectedResponseLength) throws IOException {
		connection.sendCommand(command);
		return getResponseAndValidate(expectedResponseType, expectedResponseLength);
	}
}
