package net.jevring.l8smartlight;

import java.awt.image.BufferedImage;

/**
 * Various utilities.
 *
 * @author markus@jevring.net
 * @created 2022-01-29 16:20
 */
class Utils {
	static BufferedImage byteArray2ByteBGRToImage(byte[] response, int offset) {
		BufferedImage image = new BufferedImage(128, 128, BufferedImage.TYPE_INT_ARGB);
		// since this image data is just the raw image data, we can't use ImageIO.read(), since that expects 
		// the image to have image headers and stuff as well. It doesn't let us just say "interpret this as 2-byte BGR"
		for (int i = offset; i < offset + 128; i += 2) {
			short color = extractShort(response, i);
			// These are 5-bit components
			int smallBlue = (color >> 10) & 0x1f;
			int smallGreen = (color >> 5) & 0x1f;
			int smallRed = (color & 0x1f);
			// scale them up to 8 bit components
			int blue = (int) (smallBlue * 1.6);
			int green = (int) (smallGreen * 1.6);
			int red = (int) (smallRed * 1.6);
			int rgb = Color.to4ByteARGB(red, green, blue);
			image.setRGB((i - offset) % 8, (i - offset) / 8, rgb);
		}
		return image;
	}

	static short extractShort(byte[] response, int offset) {
		return (short) ((response[offset] << 8) & 0x0000ff00 | (response[offset + 1]) & 0x000000ff);
	}

	static int imageTo2ByteBGRByteArray(BufferedImage image, byte[] imageBytes, int offset) {
		for (int y = 0; y < image.getHeight(); y++) {
			for (int x = 0; x < image.getWidth(); x++) {
				int pixel = image.getRGB(x, y);
				// getRGB() uses the color model to get us the component, and the output is ARGB
				// skip the alpha channel, as we don't care
				int red = (pixel & 0x00ff0000) >> 16;
				int green = (pixel & 0x0000ff00) >> 8;
				int blue = (pixel & 0x000000ff);
				// convert it to 2-byte 555 BGR
				// scale each component down from 8 bits to 5 bits, then shift it in to where it belongs in the byte.
				// 0x1f is 0b00011111
				// todo: this is probably not correct. This just discards the bits without actually scaling them from 8 bits to 5 bits.
				//  We should divide by 1.6 (multiply by 0.625, since multiplication is faster)
				int smallBlue = (blue & 0x1f) << 10;
				int smallGreen = (green & 0x1f) << 5;
				int smallRed = (red & 0x1f);
				int packedTwoByteBGR = smallBlue | smallGreen | smallRed;

				// the packed format is *always* 64 2-byte color values, no matter how large the image actually is.
				int gridOffset = (y * 16) + (x * 2);
				int position = offset + gridOffset;
				imageBytes[position] = (byte) ((packedTwoByteBGR >> 8) & 0x000000ff);
				imageBytes[position + 1] = (byte) (packedTwoByteBGR & 0x000000ff);
			}
		}
		// todo: better API for this method
		return offset + 128;
	}
}
