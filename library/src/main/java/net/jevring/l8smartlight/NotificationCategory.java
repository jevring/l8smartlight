package net.jevring.l8smartlight;

/**
 * Notification categories from the <a href="http://l8smartlight.com/dev/slcp/1.0/">SLCP 1.0</a>
 *
 * @author markus@jevring.net
 * @created 2022-01-15 15:56
 */
public enum NotificationCategory {
	OTHER(0),
	INCOMING_CALL(1),
	MISSED_CALL(2),
	VOICE_MAIL(3),
	SOCIAL(4),
	SCHEDULE(5),
	EMAIL(6),
	NEWS(7),
	HEALTH_AND_FITNESS(8),
	BUSINESS_AND_FINANCE(9),
	LOCATION(10),
	ENTERTAINMENT(11),
	UNKNOWN(255),
	;

	private final byte value;

	NotificationCategory(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}
}
