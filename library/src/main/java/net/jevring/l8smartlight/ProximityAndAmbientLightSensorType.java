package net.jevring.l8smartlight;

/**
 * @author markus@jevring.net
 */
public enum ProximityAndAmbientLightSensorType {
	AMBIENT_LIGHT(1),
	PROXIMITY(0);

	private final byte value;

	ProximityAndAmbientLightSensorType(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}
}
