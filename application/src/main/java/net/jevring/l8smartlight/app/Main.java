package net.jevring.l8smartlight.app;

import com.profesorfalken.jsensors.JSensors;
import com.profesorfalken.jsensors.model.components.*;
import com.profesorfalken.jsensors.model.sensors.Fan;
import com.profesorfalken.jsensors.model.sensors.Load;
import com.profesorfalken.jsensors.model.sensors.Temperature;
import net.jevring.l8smartlight.L8SmartLight;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Properties;

/**
 * Launcher for the app. Entry point for the command-line.
 *
 * @author markus@jevring.net
 * @created 2022-01-09 21:59
 */
public class Main {
	public static void experiment() {

		try {
			Properties properties = new Properties();
			try {
				properties.load(Files.newInputStream(Paths.get("config.properties")));
			} catch (IOException e) {
				System.err.println("Could not find config.properties. Assuming COM4");
			}

			L8SmartLight l8SmartLight = L8SmartLight.connectToSerialPort(properties.getProperty("port"));
			l8SmartLight.wipeUserMemory();

			// todo: experiment with all the calls
			System.out.println("l8SmartLight.getBatteryInfo() = " + l8SmartLight.getBatteryInfo());
			System.out.println("l8SmartLight.getChargingStatus() = " + l8SmartLight.getChargingStatus());
			System.out.println("l8SmartLight.getUid() = " + Arrays.toString(l8SmartLight.getUid()));
			System.out.println("l8SmartLight.getVersions() = " + l8SmartLight.getVersions());
			System.out.println("l8SmartLight.getVBusVoltage() = " + l8SmartLight.getVBusVoltage());
			System.out.println("l8SmartLight.getMcuTemperature() = " + l8SmartLight.getMcuTemperature());
			System.out.println("l8SmartLight.isButtonPressed() = " + l8SmartLight.isButtonPressed());
			System.out.println("l8SmartLight.getOrientation() = " + l8SmartLight.getOrientation());
			System.out.println("l8SmartLight.getTemperatureCelsius() = " + l8SmartLight.getTemperatureCelsius());
			System.out.println("l8SmartLight.getAccelerometerValues() = " + l8SmartLight.getAccelerometerValues());
			System.out.println("l8SmartLight.getNoiseLevel() = " + l8SmartLight.getNoiseLevel());
			System.out.println("l8SmartLight.getAmbientLight() = " + l8SmartLight.getAmbientLight());
			System.out.println("l8SmartLight.getProximity() = " + l8SmartLight.getProximity());
			System.out.println("l8SmartLight.getSensorThresholds() = " + l8SmartLight.getSensorThresholds());


			l8SmartLight.showImage(ImageIO.read(Main.class.getResourceAsStream("/colorful_m.png")));
			/*
			l8SmartLight.setOrientation(Orientation.DOWN);
			Thread.sleep(1_000);
			l8SmartLight.setOrientation(Orientation.UP);
			Thread.sleep(1_000);
			l8SmartLight.setOrientation(Orientation.LEFT);
			Thread.sleep(1_000);
			l8SmartLight.setOrientation(Orientation.RIGHT);
			Thread.sleep(1_000);
			l8SmartLight.setBrightness(Brightness.LOW);
			Thread.sleep(1_000);
			l8SmartLight.setBrightness(Brightness.MEDIUM);
			Thread.sleep(1_000);
			l8SmartLight.setBrightness(Brightness.HIGH);
			Thread.sleep(1_000);
			 */
			ImageIO.write(l8SmartLight.takeScreenshot(), "png", new File("screenshot.png"));
			
			
			

			/*
			l8SmartLight.deleteStoredNotification();
			int numberOfNotificationAppsStored = l8SmartLight.getNumberOfNotificationAppsStored();
			System.out.println("l8SmartLight.getNumberOfNotificationAppsStored() = " + numberOfNotificationAppsStored);
			for (int i = 0; i < numberOfNotificationAppsStored; i++) {
				System.out.println("l8SmartLight.getNotificationAppInformation(" + i + ") = " + l8SmartLight.getNotificationAppInformation(i));
			}
			l8SmartLight.storeNotification("/weird_rainbow.png", "rainbow", new Color(0xff, 0x77, 0x00), true);
			 */
			//l8SmartLight.showStoredNotification("rainbow", NotificationEvent.ADDED, NotificationCategory.EMAIL);
			// todo: have custom methods for things like incoming call
			//l8SmartLight.showStoredNotification("", NotificationEvent.ADDED, NotificationCategory.INCOMING_CALL);
			//l8SmartLight.showStoredNotification("", NotificationEvent.REMOVED, NotificationCategory.INCOMING_CALL);

			/*
			l8SmartLight.clearScreen();
			Thread.sleep(1_000);
			BufferedImage image = ImageIO.read(Main.class.getResourceAsStream("/colorful_m.png"));
			l8SmartLight.drawImage(image);
			*/

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		if (args.length > 0 && "--dump-sensors".equalsIgnoreCase(args[0])) {
			System.out.println("Initializing sensors. This might take a while");
			dumpSensors();
		} else if (args.length > 0 && "--experiment".equalsIgnoreCase(args[0])) {
			experiment();
		} else {
			Monitor monitor = new Monitor();
			monitor.run();
		}
	}

	private static void dumpSensors() {
		try {
			Components components = JSensors.get.components();
			for (Gpu gpu : components.gpus) {
				dumpComponent("gpu", gpu);
			}
			for (Cpu cpu : components.cpus) {
				dumpComponent("cpu", cpu);

			}
			for (Disk disk : components.disks) {
				dumpComponent("disk", disk);
			}
			for (Mobo mobo : components.mobos) {
				dumpComponent("motherboard", mobo);
			}
		} finally {
			// It seems like the JSensors library is keeping us alive or something, so we have to kill it
			System.exit(0);
		}
	}

	private static void dumpComponent(String prefix, Component component) {
		System.out.println(prefix + "=" + component.name);
		for (Temperature temperature : component.sensors.temperatures) {
			System.out.println(prefix + ".temperature=" + temperature.name);
		}
		for (Fan fan : component.sensors.fans) {
			System.out.println(prefix + ".fan=" + fan.name);
		}
		for (Load load : component.sensors.loads) {
			System.out.println(prefix + ".load=" + load.name);
		}
	}
}
