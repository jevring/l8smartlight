package net.jevring.l8smartlight.app;

import com.profesorfalken.jsensors.JSensors;
import com.profesorfalken.jsensors.model.components.Components;
import com.profesorfalken.jsensors.model.components.Cpu;
import com.profesorfalken.jsensors.model.components.Gpu;
import com.profesorfalken.jsensors.model.sensors.Load;
import com.profesorfalken.jsensors.model.sensors.Temperature;

import java.util.Properties;

/**
 * A wrapper around reading sensors. Call {@link #measure()} each time you want new values,
 * then get the values via the getters.
 * todo: should we change this to have measure() return a snapshot of the sensors?
 *
 * @author markus@jevring.net
 * @created 2022-01-22 22:38
 */
public class Sensors {
	private final Properties properties;
	private boolean initialized = false;
	private double cpuLoad;
	private double cpuTemperature;
	private double gpuLoad;
	private double gpuTemperature;

	public Sensors(Properties properties) {
		this.properties = properties;
	}

	public void measure() {
		long start = System.currentTimeMillis();
		if (!initialized) {
			System.out.println("Initializing sensors. This might take a while");
			initialized = true;
		}
		// NOTE: components() must be called EVERY TIME we want a new measurement.
		Components components = JSensors.get.components();
		long end = System.currentTimeMillis();
		System.out.println("JSensors read in " + (end - start) + " milliseconds");

		String watchedGpu = properties.getProperty("gpu");
		String watchedGpuTemperature = properties.getProperty("gpu.temperature");
		String watchedGpuLoad = properties.getProperty("gpu.load");
		for (Gpu gpu : components.gpus) {
			if (!gpu.name.equalsIgnoreCase(watchedGpu)) {
				continue;
			}
			for (Load load : gpu.sensors.loads) {
				if (!load.name.equals(watchedGpuLoad)) {
					continue;
				}
				System.out.printf("GPU %s load %s : %f%n", gpu.name, load.name, load.value);
				gpuLoad = load.value;
			}
			for (Temperature temperature : gpu.sensors.temperatures) {
				if (!temperature.name.equals(watchedGpuTemperature)) {
					continue;
				}
				System.out.printf("GPU %s temperature %s : %f%n", gpu.name, temperature.name, temperature.value);
				gpuTemperature = temperature.value;
			}
		}


		String watchedCpu = properties.getProperty("cpu");
		String watchedCpuTemperature = properties.getProperty("cpu.temperature");
		String watchedCpuLoad = properties.getProperty("cpu.load");
		for (Cpu cpu : components.cpus) {
			if (!cpu.name.equalsIgnoreCase(watchedCpu)) {
				continue;
			}
			for (Load load : cpu.sensors.loads) {
				if (!load.name.equals(watchedCpuLoad)) {
					continue;
				}
				System.out.printf("CPU %s load %s : %f%n", cpu.name, load.name, load.value);
				cpuLoad = load.value;
			}
			for (Temperature temperature : cpu.sensors.temperatures) {
				if (!temperature.name.equals(watchedCpuTemperature)) {
					continue;
				}
				System.out.printf("CPU %s temperature %s : %f%n", cpu.name, temperature.name, temperature.value);
				cpuTemperature = temperature.value;
			}
		}
	}

	public double getCpuLoad() {
		return cpuLoad;
	}

	public double getCpuTemperature() {
		return cpuTemperature;
	}

	public double getGpuLoad() {
		return gpuLoad;
	}

	public double getGpuTemperature() {
		return gpuTemperature;
	}
}
