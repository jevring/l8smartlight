package net.jevring.l8smartlight.app;

import net.jevring.l8smartlight.Color;
import net.jevring.l8smartlight.L8SmartLight;

import javax.imageio.ImageIO;
import java.io.IOException;

/**
 * An animation mimicing a moving fire.
 *
 * @author markus@jevring.net
 * @created 2022-01-23 15:22
 */
public class FlameAnimation {
	private static final Color ORANGE = new Color(255, 153, 63);
	private static final Color OFF = new Color(0, 0, 0);
	private final int animationIndex;

	private FlameAnimation(int animationIndex) {
		this.animationIndex = animationIndex;
	}

	public static FlameAnimation prepare(L8SmartLight l8SmartLight) throws IOException {
		int frame1Index = l8SmartLight.storeAnimationFrame(ImageIO.read(FlameAnimation.class.getResourceAsStream("/flame_1.png")));
		int frame2Index = l8SmartLight.storeAnimationFrame(ImageIO.read(FlameAnimation.class.getResourceAsStream("/flame_2.png")));
		int animationIndex = l8SmartLight.storeAnimation(frame1Index, 3, frame2Index, 3);

		return new FlameAnimation(animationIndex);
	}

	public void display(L8SmartLight l8SmartLight) throws IOException {
		l8SmartLight.playAnimation(animationIndex, true);
		l8SmartLight.setSuperLed(ORANGE);
	}

	public void stop(L8SmartLight l8SmartLight) throws IOException {
		l8SmartLight.stopAnimation();
		l8SmartLight.setSuperLed(OFF);
	}
}
