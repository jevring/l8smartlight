package net.jevring.l8smartlight.app;

import net.jevring.l8smartlight.Brightness;
import net.jevring.l8smartlight.Color;
import net.jevring.l8smartlight.L8SmartLight;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Monitors various sensors etc and displays their values as bars on the L8SmartLight.
 * Will alert graphically when seeing high load or high temperature.
 *
 * @author markus@jevring.net
 * @created 2022-01-22 21:34
 */
public class Monitor implements Runnable {
	private static final Color RED = new Color(255, 0, 0);
	private static final Color GREEN = new Color(0, 255, 0);
	private static final Color BLUE = new Color(0, 0, 255);
	private static final Color ORANGE = new Color(255, 153, 63);
	private static final int GPU_FIRE_LIMIT = 80;
	private static final int CPU_FIRE_LIMIT = 60;
	private final Properties properties;

	private L8SmartLight l8SmartLight;
	private boolean requiresInitialization = true;
	private FlameAnimation flameAnimation = null;
	private volatile boolean running = false;
	private volatile Thread currentThread;

	public Monitor() {
		this.properties = new Properties();
		try {
			properties.load(Files.newInputStream(Paths.get("config.properties")));
		} catch (IOException e) {
			System.err.println("Could not find config.properties. Assuming COM4");
		}
	}

	public void stop() {
		this.running = false;
		this.currentThread.interrupt();
	}

	public void run() {
		l8SmartLight = L8SmartLight.connectToSerialPort(properties.getProperty("port"));
		Sensors sensors = new Sensors(properties);
		this.running = true;
		this.currentThread = Thread.currentThread();

		while (running) {
			try {
				sensors.measure();

				if (requiresInitialization) {
					// Let's hope we don't wear the memory out too much. 
					// It would be nicer if we could reliably store the state, but since we can't 
					// be sure whatever else is on here, and what stuff is at what index, it's easier
					// to just wipe the memory and assume we're the only users of this thing
					requiresInitialization = false;
					l8SmartLight.wipeUserMemory();
					flameAnimation = FlameAnimation.prepare(l8SmartLight);
					l8SmartLight.setBrightness(Brightness.LOW);
				}


				if (sensors.getCpuTemperature() > CPU_FIRE_LIMIT || sensors.getGpuTemperature() > GPU_FIRE_LIMIT) {
					l8SmartLight.setBrightness(Brightness.HIGH);
					flameAnimation.display(l8SmartLight);
				} else {
					l8SmartLight.setBrightness(Brightness.LOW);
					flameAnimation.stop(l8SmartLight);
					BufferedImage image = new BufferedImage(8, 8, BufferedImage.TYPE_INT_ARGB);
					drawBar(sensors.getCpuLoad(), 100d, 0, BLUE, image);
					drawBar(sensors.getCpuTemperature(), 90d, 1, GREEN, image);
					drawBar(sensors.getGpuLoad(), 100d, 6, BLUE, image);
					drawBar(sensors.getGpuTemperature(), 90d, 7, GREEN, image);
					l8SmartLight.showImage(image);
				}

				Thread.sleep(5_000);
			} catch (IOException e) {
				System.err.println("Looks like the smart light went away. Retrying");
				e.printStackTrace();
				l8SmartLight.attemptToReconnect();
				requiresInitialization = true;
			} catch (InterruptedException e) {
				System.out.println("Thread was interrupted while sleeping. No biggie, we'll just continue.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void drawBar(double value, double max, int column, Color baseColor, BufferedImage image) {
		int barHeight = (int) Math.round((value / max) * 8d);
		for (int i = 0; i < barHeight; i++) {
			int rgb = baseColor.to4ByteARGB();
			if (i == 7) {
				rgb = RED.to4ByteARGB();
			}
			if (i == 6 || i == 5) {
				rgb = ORANGE.to4ByteARGB();
			}
			image.setRGB(column, 7 - i, rgb);
		}
	}
}
