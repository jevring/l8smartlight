# L8SmartLight

This is a library for communicating with the L8SmartLight via COM ports in Java.
The library is accompanied by an application that lets you monitor temperatures and load of your machine.
The library and application are both pre-alpha stage at the moment, though I hope to make them better, assuming I don't just lose interest and quit.
It's a synchronous library, so you'll send requests and get responses (where applicable) in a single call.
This is unlike the "official" libraries that were, at best, half-baked, and just had sending and receiving as two asynchronous parts. 
Also, it only worked for android, assuming it ever worked, and only via bluetooth, and only a handful of things was supported.

We make use of some external stuff:

Professorfalken's sensor library.

fazecast's serial port library

A random homebrew algorithm for CRC8-CCITT made for the Nintendo 3ds. You'd think that this was well documented around the internet, but no. 
This was the only thing I could find. I plan to rewrite this one day. Not because it doesn't work, but because I've just copied and pasted it
to not spend my entire project time worrying about crcs. I'm sure it's fine, but I'd like to have my own, or a "real" library from maven central.
It's amazing that either the l8 people have chosen a weird and uncommon crc8 implementation, or that nobody has chosen to properly document it for 8 bits.
Anyway, enough rant, that might get replaced one day. It's linked in the class itself.
